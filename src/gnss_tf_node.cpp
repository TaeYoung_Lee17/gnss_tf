#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <tf/transform_broadcaster.h>
#include <diagnostic_updater/diagnostic_updater.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <geotrans/geotrans.h>
#include <string>

ros::Publisher _pubOdom;
ros::Publisher _pubImu;
tf::TransformBroadcaster* _pubTF;

geometry_msgs::Quaternion _imuOrientation;
geometry_msgs::TwistWithCovariance _twistImu;
sensor_msgs::NavSatFix _navSatFix;
bool _publish_odom_tf_;
double _globalOffsetX;
double _globalOffsetY;
std::string _gnss_frame;
bool _use_altitude_;
GeoTrans _geotrans;
std::string _paramProjection;

void callback_navfix(const sensor_msgs::NavSatFixConstPtr& msg)
{
  ros::Time t = ros::Time::now();
  _navSatFix = *msg;

  double lat_deg, lon_deg, alt;
  lat_deg = msg->latitude;
  lon_deg = msg->longitude;
  alt = msg->altitude;

  // Lat/Lon to TM
  double tm_x = 0, tm_y = 0;  
  if (!_geotrans.toTM(lon_deg, lat_deg, tm_x, tm_y)) {
    return;
  }

  // Pose (ENU)
  double tx, ty, tz;
  tx = tm_x - _globalOffsetX;
  ty = tm_y - _globalOffsetY;
  if(_use_altitude_)
    // cloud map 의 높이와 ins 의 고도에 차이가 있다.
    // 지구상에서 높이(해발고도)를 측정하는 기준이 되는 평균 해수면과
    // GPS 높이의 기준이 되는 타원체고의 차이 (20~30m)
//    tz = alt + 22;
    tz = alt - 5;
  else
    tz = 0;

  // Publish TF
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.stamp = t;
  odom_trans.header.frame_id = "odom";
  odom_trans.child_frame_id = _gnss_frame;
  odom_trans.transform.translation.x = tx;
  odom_trans.transform.translation.y = ty;
  odom_trans.transform.translation.z = tz;
  odom_trans.transform.rotation = _imuOrientation;
  if (_publish_odom_tf_)
    _pubTF->sendTransform(odom_trans);


  // Publish Odometry
  nav_msgs::Odometry odom;
  odom.header.stamp = t;
  odom.header.frame_id = "odom";
  odom.child_frame_id = _gnss_frame;
  odom.pose.pose.position.x = tx;
  odom.pose.pose.position.y = ty;
  odom.pose.pose.position.z = tz;
  odom.pose.pose.orientation = _imuOrientation;
  odom.twist = _twistImu;
  _pubOdom.publish(odom);
}

void callback_imu(const sensor_msgs::ImuConstPtr& msg)
{
  ros::Time t = ros::Time::now();

  sensor_msgs::Imu imu;
  imu = *msg;
  imu.header.frame_id = "base_footprint";
  imu.header.stamp = t;

  _imuOrientation = imu.orientation;

  _pubImu.publish(imu);
}

void callback_vel(const geometry_msgs::TwistWithCovarianceStampedConstPtr& msg)
{
  _twistImu = msg->twist;
}

void diagnostic_status(diagnostic_updater::DiagnosticStatusWrapper &stat)
{
  ros::Time t = ros::Time::now();

  bool projection_ok = _geotrans.valid();

  bool navsatfix_ok = ((t - _navSatFix.header.stamp).toSec() < 1);   

  bool tm_ok = false;
  double tm_x = 0, tm_y = 0;
  if (navsatfix_ok && projection_ok) {
    tm_ok = _geotrans.toTM(_navSatFix.longitude, _navSatFix.latitude, tm_x, tm_y);
  }

  if (!projection_ok) {
    stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Projection Error");
  }
  else if (!navsatfix_ok) {
    stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Receive NavSatFix (Timeout)");
  }
  else if (!tm_ok) {
    stat.summary(diagnostic_msgs::DiagnosticStatus::ERROR, "Coordinate Transform Fail");
  }
  else {
    stat.summary(diagnostic_msgs::DiagnosticStatus::OK, "OK");
  }

  stat.add("Projection", _paramProjection);

  if (navsatfix_ok) {
    stat.addf("Longitude (deg)", "%.6f", _navSatFix.longitude);
    stat.addf("Latitude (deg)", "%.6f", _navSatFix.latitude);
    stat.addf("Altitude (m)", "%.2f", _navSatFix.altitude);
  }
  else {
    stat.add("Longitude (deg)", "-");
    stat.add("Latitude (deg)", "-");
    stat.add("Altitude (m)", "-");
  }

  if (tm_ok) {
    stat.addf("X (m)", "%.2f", tm_x);
    stat.addf("Y (m)", "%.2f", tm_y);
  }
  else {
    stat.add("X (m)", "-");
    stat.add("Y (m)", "-");
  }

  stat.addf("Global Offset X (m)", "%.2f", _globalOffsetX);
  stat.addf("Global Offset Y (m)", "%.2f", _globalOffsetY);
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "gnss_tf_node");

  ROS_INFO("start gnss_tf_node");

  ros::NodeHandle p_nh("~");
  p_nh.param("publish_odom_tf", _publish_odom_tf_, true);
  if (_publish_odom_tf_)
    ROS_INFO("PUBLISHING odom->base_footprint tf");
  else
    ROS_INFO("NOT PUBLISHING odom->base_footprint tf");
  p_nh.param("gnss_frame", _gnss_frame, std::string("base_footprint"));
  p_nh.param<bool>("use_altitude", _use_altitude_, false);

  ros::NodeHandle nh;
  nh.param("global_offset_x", _globalOffsetX, 0.0);
  nh.param("global_offset_y", _globalOffsetY, 0.0);
  nh.param("projection", _paramProjection, std::string(""));

  if (_geotrans.init(_paramProjection)) {
    ROS_INFO("[gnss_tf_node] Set Projection OK (%s)", _paramProjection.c_str());
  }
  else {
    ROS_ERROR("[gnss_tf_node] Set Projection ERROR (%s)", _paramProjection.c_str());
  }

  _pubOdom = nh.advertise<nav_msgs::Odometry>("odom", 100);
  _pubImu = nh.advertise<sensor_msgs::Imu>("gnss_imu", 100);
  _pubTF = new tf::TransformBroadcaster;

  ros::Subscriber subNavFix = nh.subscribe<sensor_msgs::NavSatFix>("FOG_INS/NavSatFix", 100, callback_navfix);
  ros::Subscriber subImu = nh.subscribe<sensor_msgs::Imu>("FOG_INS/Imu", 100, callback_imu);
  ros::Subscriber subTwist = nh.subscribe<geometry_msgs::TwistWithCovarianceStamped>("FOG_INS/Twist", 100, callback_vel);

  diagnostic_updater::Updater diag_updater;
  diag_updater.setHardwareID("gnss_tf_node");
  diag_updater.add("Status", diagnostic_status);

  diagnostic_updater::Updater* diag_updater_ptr = &diag_updater;
  ros::Timer timer_diag_updater = nh.createTimer(ros::Duration(0.1),  
                                                 [diag_updater_ptr](const ros::TimerEvent& event){
    diag_updater_ptr->force_update();
  });

  //_imuOrientation.w = 1;
  ros::spin();

  ROS_INFO("destroy gnss_tf_node");

  return 0;
}
